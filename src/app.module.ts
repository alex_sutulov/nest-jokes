import { EventsModule } from './sockets/events.module';
import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { AppController } from './app.controller';
import { JokesModule } from './jokes/jokes.module';

@Module({
  imports: [
    MongooseModule.forRoot('mongodb://localhost:27017/jokes', { useNewUrlParser: true }),
    JokesModule,
    EventsModule
  ],
  controllers: [AppController],
})
export class AppModule {
}
