import { Controller, Get } from '@nestjs/common';

@Controller('')
export class AppController {
  constructor() {}

  @Get()
  getJoke() {
    return {
      code: '200',
      message: 'App works',
    };
  }
}
