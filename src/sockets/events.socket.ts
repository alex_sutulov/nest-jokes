import {
    SubscribeMessage,
    WebSocketGateway,
    WebSocketServer,
    OnGatewayConnection,
    OnGatewayDisconnect,
    OnGatewayInit,
} from '@nestjs/websockets';
import { Server } from 'ws';
import { Socket } from 'socket.io';

@WebSocketGateway({ port: 3001, transports: ['websocket'], path: '/ws' })
export class EventsGateway implements OnGatewayConnection, OnGatewayDisconnect, OnGatewayInit {

    @WebSocketServer() private server: Server;
    wsClients = [];
    userInc = 0;

    afterInit() {
        this.server.emit('testing', { do: 'stuff' });
    }

    private generateRandomId(): string {
        return Number.parseInt(String(Math.random() * 10 * 5000)).toString();
    }

    handleConnection(client: Socket) {
        client.id = this.generateRandomId();
        console.log('connected client: ', client.id);
        this.broadcast('connect', { clientId: client.id });
        this.wsClients.push(client);
    }

    handleDisconnect(client: Socket) {
        this.wsClients = this.wsClients.filter(wsClient => wsClient !== client);
        this.broadcast('disconnect', { deleted: client.id });
        console.log('disconnect', client.id);
    }
    private broadcast(event, message: any) {
        const broadCastMessage = JSON.stringify({ event, data: message });
        for (let c of this.wsClients) {
            c.send(broadCastMessage);
        }
    }

    @SubscribeMessage('my-event')
    onChgEvent(client: Socket, payload: any) {
        payload.clientId = client.id;
        this.broadcast('my-event', payload);
    }
}
