import { Module } from '@nestjs/common';
import { EventsGateway } from './events.socket';

@Module({
  providers: [EventsGateway],
})
export class EventsModule {}