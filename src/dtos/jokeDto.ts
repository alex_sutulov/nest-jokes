import { IJoke } from './../interfaces/joke';

export class Joke {

    categories: string[];
    created_at: string;
    icon_url: string;
    id: string;
    updated_at: string;
    url: string;
    value: string;

    constructor(props: IJoke) {
        this.categories = props.categories;
        this.created_at = props.created_at;
        this.icon_url = props.icon_url;
        this.id = props.id;
        this.updated_at = props.updated_at;
        this.url = props.url;
        this.value = props.value;
    }
}