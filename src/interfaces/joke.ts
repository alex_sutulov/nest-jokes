import { Document } from 'mongoose';

export interface IJoke extends Document {
  categories: string[];
  created_at: string;
  icon_url: string;
  id: string;
  updated_at: string;
  url: string;
  value: string;
}
