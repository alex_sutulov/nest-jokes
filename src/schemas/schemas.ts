import * as mongoose from 'mongoose';
import { autoIncrement } from 'mongoose-plugin-autoinc';

export const JokeSchema = new mongoose.Schema({
  categories: Array,
  created_at: String,
  icon_url: String,
  id: String,
  updated_at: String,
  url: String,
  value: String,
});

export const UserSchema = new mongoose.Schema({
  name: String,
  email: { type: String, required: true },
  age: String,
  lastLogin: Date,
  password: { type: String, required: true },
  humanId: { type: Number, unique: true },
});

UserSchema.plugin(autoIncrement, {
  model: 'User',
  field: 'humanId',
  startAt: 10000,
  unique: true,
  incrementBy: 1,
 });
