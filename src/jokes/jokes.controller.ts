import { JokesService } from './jokes.service';
import { Controller, Get, Param, Post, Delete, Body, Inject } from '@nestjs/common';
import { IJoke } from '../interfaces/joke';

@Controller('jokes')
export class JokesController {
  constructor(
    @Inject(JokesService) private readonly jokesService: JokesService
  ) { }

  @Get('/quantity')
  async getJokesLength() {
    const quantity = await this.jokesService.getJokesQuantity();
    return { quantity };
  }

  @Get('/search/:field/:term')
  searchJoke(
    @Param('field') field: string,
    @Param('term') term: string,
  ): Promise<IJoke[]> {
    return this.jokesService.searchJoke(field, term);
  }

  @Get(':id')
  getJoke(@Param('id') id: string) {
    return this.jokesService.getJoke(id);
  }

  @Get('/page/:page/limit/:limit')
  searchByPage(
    @Param('page') page: number,
    @Param('limit') limit: number,
  ) {
    limit = +limit;
    return this.jokesService.getJokesByPage(page, limit);
  }

  @Get()
  getAllJokes() {
    return this.jokesService.getAllJokes();
  }

  @Post('/search')
  getExtraSearch(@Body() params: object) {
    return this.jokesService.extraSearchJoke(params);
  }

  @Post()
  getNewJokes() {
    return this.jokesService.postNewJoke();
  }

  @Delete(':id')
  deleteJoke(@Param('id') id: string) {
    return this.jokesService.deleteJoke(id);
  }
}
