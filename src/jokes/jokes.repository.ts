import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { IJoke } from '../interfaces/joke';
import { Joke } from 'src/dtos/jokeDto';

@Injectable()
export class JokesRepository {

  constructor(
    @InjectModel('Joke') private readonly jokeModel: Model<IJoke>,
  ) {
  }

  async findJokesByField(field: string, term: string) {
    return await this.jokeModel.find({ [field]: { $regex: term, $options: 'i' } }).exec();
  }

  async findAllJokes() {
    return await this.jokeModel.find({}).exec();
  }

  async findOneJoke(id: string) {
    try {
      return await this.jokeModel.findOne({ $or: [{ _id: id }, { id }] }).exec();
    } catch (err) {
      return { ...err };
    }
  }

  async insertOne(joke: Joke): Promise<void> {
    await this.jokeModel.insertMany(joke);
  }

  async getJokesQuantity(): Promise<number> {
    return await this.jokeModel.countDocuments({}).exec();
  }

  async deleteJoke(id: string) {
    return await this.jokeModel.findOneAndDelete({ id }).exec();
  }

  async findJokesBySearchParams(params: object) {
    return await this.jokeModel.find(params).exec();
  }

  async findJokesByPage(page: number, limit: number = 5) {
    return await this.jokeModel
      .find()
      .skip(page * limit)
      .limit(limit)
      .exec()
  }
}
