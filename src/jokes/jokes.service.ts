import { Joke } from './../dtos/jokeDto';
import { IJoke } from './../interfaces/joke';
import { Inject, Injectable } from '@nestjs/common';
import axios from 'axios';
import config from '../config/env';
import { JokesRepository } from './jokes.repository';

@Injectable()
export class JokesService {
  constructor(
    @Inject(JokesRepository) private readonly jokesRepository: JokesRepository,
  ) { }

  async postNewJoke(): Promise<Joke> {
    const { url } = config;
    const response = await axios.get(url);
    const jokeJson = response.data as IJoke;
    const joke = new Joke(jokeJson);
    const doc = await this.jokesRepository.findOneJoke(joke.id);
    if (doc === null) {
      await this.jokesRepository.insertOne(joke);
    }
    return joke;
  }

  async getAllJokes() {
    const [jokes, quantity] = await Promise.all([
      this.jokesRepository.findAllJokes(),
      this.jokesRepository.getJokesQuantity()
    ]);
    return { jokes, quantity };
  }

  async getJokesByPage(page: number, limit: number = 5) {
    const jokes = await this.jokesRepository.findJokesByPage(page, limit);
    const quantity = await this.jokesRepository.getJokesQuantity();
    const pages = Math.floor(quantity / limit);
    return { pages, jokes };
  }

  getJoke(id: string): Promise<Joke> {
    return this.jokesRepository.findOneJoke(id);
  }

  searchJoke(field: string, term: string) {
    return this.jokesRepository.findJokesByField(field, term);
  }

  deleteJoke(id: string): Promise<Joke> {
    return this.jokesRepository.deleteJoke(id);
  }

  async extraSearchJoke(searchParams: object): Promise<Joke[]> {
    const keys = Object.keys(searchParams);
    for (const key of keys) {
      searchParams[key] = { $regex: searchParams[key], $options: 'i' };
    }
    return this.jokesRepository.findJokesBySearchParams(searchParams);
  }

  getJokesQuantity() {
    return this.jokesRepository.getJokesQuantity();
  }
}
