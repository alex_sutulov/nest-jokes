import { JokesRepository } from './jokes.repository';
import { JokesController } from './jokes.controller';
import { JokesService } from './jokes.service';
import { JokeSchema } from './../schemas/schemas';
import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: 'Joke', schema: JokeSchema },
    ]),
  ],
  controllers: [
    JokesController
  ],
  providers: [
    JokesService,
    JokesRepository
  ],
})
export class JokesModule {
}
