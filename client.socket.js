const WebSocket = require('ws');
const inspect = require('util').inspect;
 
const ws = new WebSocket('ws://localhost:3001/ws');
 
ws.on('open', function open() {
    console.log('Connection opened');
});
 
ws.on('message', function incoming(message) {
  const { event, data } = JSON.parse(message);
  console.log(`Event => ${event}, data => ${inspect(data)}`);
});

ws.on('close', () => {
  console.log('Connection closed');
});